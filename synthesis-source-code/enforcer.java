import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

class Transition {
    int source;
    String event;
    Set <String> guard; 
    int target;
    String type;
    Transition (int isource, String ievent, Set <String> iguard, int itarget, String itype) {
        source = isource;
        event = ievent;
        guard = iguard;
        target = itarget;
        type = itype;
    }
}
class With {
    String event;
    String data;
    With (String iEvent, String iData){
        event = iEvent;
        data = iData;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((event == null) ? 0 : event.hashCode()) + ((data == null) ? 0 : data.hashCode());
      return result;
    }

    @Override
    public boolean equals(Object o) {
        With tmpWith = (With) o;
        if (event.equals(tmpWith.event) && data.equals(tmpWith.data)) {
            return true;
        } else {
            return false;
        }
    }
}

class EnforcerFB {
    String name;
    EnforcerInterfaces interfaces;
    Map <EnforcerState, Set<EnforcerTransition>> ecc;
    Set <EnforcerAlgo> algorithms;

    EnforcerFB (String iName, EnforcerInterfaces iInterfaces,
                Map <EnforcerState, Set<EnforcerTransition>> iEcc, Set <EnforcerAlgo> iAlgorithms) {
        name =  iName;
        interfaces = iInterfaces;
        ecc = iEcc;
        algorithms = iAlgorithms;
    }
}

class EnforcerInterfaces {
    Set <String> eventInputs;
    Set <String> eventOutputs;
    Set <String> dataInputs;
    Set <String> dataOutputs;
    Set <With> withInputs;
    Set <With> withOutputs;
    EnforcerInterfaces (Set <String> iEventInputs, Set <String> iEventOutputs,
                        Set <String> iDataInputs, Set <String> iDataOutputs,
                        Set <With> iWithInputs, Set <With> iWithOutputs) {
        eventInputs = iEventInputs;
        eventOutputs = iEventOutputs;
        dataInputs = iDataInputs;
        dataOutputs = iDataOutputs;
        withInputs = iWithInputs;
        withOutputs = iWithOutputs;
    }
}

class EnforcerAlgo {
    String ID;
    String algo;
    EnforcerAlgo (String iID, String iAlgo) {
        ID = iID;
        algo = iAlgo;
    }
}

class EnforcerAction {
    String algoID;
    String eventOut;
    EnforcerAction (String iAlgoID, String iEventOut) {
        algoID = iAlgoID;
        eventOut = iEventOut;
    }
}

class EnforcerState {
    private static final AtomicInteger idx = new AtomicInteger(-1); 
    int state;
    Set <EnforcerAction> actions;
    EnforcerState (Set <EnforcerAction> iActions) {
        state = idx.incrementAndGet();
        actions = iActions;
    }
    
    @Override
    public boolean equals(Object o) {
        EnforcerState tmpState = (EnforcerState) o;
        if (state == tmpState.state) {
            return true;
        } else {
            return false;
        }
    }
}

class EnforcerTransition {
    EnforcerState source;
    String event;
    Set <String> guard; 
    EnforcerState target;
    boolean isEmpty;
    EnforcerTransition (EnforcerState isource, EnforcerState itarget, boolean iIsEmpty) {
        source = isource;
        event = "";
        guard = new HashSet<String>();
        target = itarget;
        isEmpty = iIsEmpty;
    }
    EnforcerTransition (EnforcerState isource, String ievent, Set <String> iguard, EnforcerState itarget, boolean iIsEmpty) {
        source = isource;
        event = ievent;
        guard = iguard;
        target = itarget;
        isEmpty = iIsEmpty;
    }
}

class enforcer {

    static void buildECC(Map <Integer, Set <Transition>> propMapIn, EnforcerState cStateEnf, int cState,
    Map <EnforcerState, Set <EnforcerTransition>> eccOut, Set <Integer> visited, Map <Integer, EnforcerState> mapStateEnf,
    Set <EnforcerAlgo> algosOut) {
        if(!visited.contains(cState)) {
            visited.add(cState);
            mapStateEnf.put(cState, cStateEnf);
            Set <EnforcerTransition> newEnfTrans = new HashSet<EnforcerTransition>();
            for (Transition tr : propMapIn.get(cState)) {
                Iterator <String> it = tr.guard.iterator();
                
                // check replacement
                int tmpRplcTarget = 1000;
                if (tr.type.equals("REPLACE")) {
                    Set <Transition> tmpTransRplc = new HashSet<Transition>();
                    for (Transition trFindRplc : propMapIn.get(cState)) {
                        if(trFindRplc.type.equals("REPLACEMENT")) {
                            tmpTransRplc.add(trFindRplc);
                        } else if (trFindRplc.type.equals("") && propMapIn.get(cState).size() == 2) {
                            tmpTransRplc.add(trFindRplc);
                        }
                    }
                    Iterator <Transition> itTrRplc = tmpTransRplc.iterator();
                    Transition tmpTrRplc = itTrRplc.next();
                    tmpRplcTarget = tmpTrRplc.target;
                    it = tmpTrRplc.guard.iterator();
                }
                
                // build action
                String tmpGuard = "";
                String tmpEnfAlgo = "";
                String tmpEnfGuard = "";
                if (it.hasNext()){ tmpGuard = it.next(); }
                tmpEnfAlgo = tmpGuard.split("=")[0] + "_O := " + tmpGuard.split("=")[1] + ";";
                tmpEnfGuard = tmpGuard.split("=")[0] + "_I = " + tmpGuard.split("=")[1];
                Set <EnforcerAlgo> tmpExistAlgo = new HashSet<EnforcerAlgo>();
                for (EnforcerAlgo enforcerAlgo : algosOut) {
                    if (enforcerAlgo.algo.equals(tmpEnfAlgo)) {
                        tmpExistAlgo.add(enforcerAlgo);
                    }
                }
                EnforcerAlgo newEnfAlgo;
                if (tmpExistAlgo.size() == 0) {
                    newEnfAlgo = new EnforcerAlgo("Algo" + (algosOut.size()+1), tmpEnfAlgo);
                    algosOut.add(newEnfAlgo);
                } else {
                    Iterator <EnforcerAlgo> itAlgo = tmpExistAlgo.iterator();
                    newEnfAlgo = itAlgo.next();
                }
                String tmpEvent = tr.event + "_O";
                EnforcerAction newEnfAction = new EnforcerAction(newEnfAlgo.ID, tmpEvent);
                
                if (visited.contains(tr.target)) {
                    EnforcerState newEnfMidSrc = new EnforcerState(new HashSet<EnforcerAction>(Arrays.asList(newEnfAction)));
                    eccOut.put(newEnfMidSrc, new HashSet<EnforcerTransition>(Arrays.asList(new EnforcerTransition(newEnfMidSrc, mapStateEnf.get(tr.target), true))));
                    newEnfTrans.add(new EnforcerTransition(cStateEnf, tr.event+"_I", new HashSet<String>(Arrays.asList(tmpEnfGuard)), newEnfMidSrc, false));
                } else if (tr.type.equals("REPLACE")){
                    it = tr.guard.iterator();
                    if (it.hasNext()){ tmpGuard = it.next(); }
                    tmpEnfGuard = tmpGuard.split("=")[0] + "_I = " + tmpGuard.split("=")[1];
                    EnforcerState newEnfMidSrc = new EnforcerState(new HashSet<EnforcerAction>(Arrays.asList(newEnfAction)));
                    eccOut.put(newEnfMidSrc, new HashSet<EnforcerTransition>(Arrays.asList(new EnforcerTransition(newEnfMidSrc, mapStateEnf.get(tmpRplcTarget), true))));
                    newEnfTrans.add(new EnforcerTransition(cStateEnf, tr.event+"_I", new HashSet<String>(Arrays.asList(tmpEnfGuard)), newEnfMidSrc, false));
                } else if (!tr.type.equals("DISCARD") || (tr.type.equals("") && tr.target != -1)){
                    EnforcerState newEnfTgt = new EnforcerState(new HashSet<EnforcerAction>(Arrays.asList(newEnfAction)));
                    newEnfTrans.add(new EnforcerTransition(cStateEnf, tr.event+"_I", new HashSet<String>(Arrays.asList(tmpEnfGuard)), newEnfTgt, false));
                    buildECC(propMapIn, newEnfTgt, tr.target, eccOut, visited, mapStateEnf, algosOut);
                }
            }
            eccOut.put(cStateEnf, newEnfTrans);
        }
    }

    static void printInterfaces (String name, Set <String> itfs) {
        System.out.print("\t" + name + " = {");
        int tmplen = itfs.size();
        for (String itfstr : itfs) {
            System.out.print(itfstr);
            tmplen--;
            if (tmplen != 0) {
                System.out.print(", ");
            }
        }
        System.out.println("}");
    }
    static void printWiths (String name, Set <With> wths) {
        System.out.print("\t" + name + " = {");
        int tmplen = wths.size();
        for (With wth : wths) {
            System.out.print(wth.event +  " - " + wth.data);
            tmplen--;
            if (tmplen != 0) {
                System.out.print(", ");
            }
        }
        System.out.println("}");
    }

    static void printECC (Map <EnforcerState, Set <EnforcerTransition>> ecc) {
        System.out.println("\nECC: ");
        for (EnforcerState st : ecc.keySet()) {
            System.out.println("\tState " + st.state);
            System.out.println("\t\tActions:");
            for (EnforcerAction ac : st.actions) {
                System.out.println("\t\t\tAlgo: " + ac.algoID + ", Event: " + ac.eventOut);
            }
            System.out.println("\t\tTransition:");
            for (EnforcerTransition tr : ecc.get(st)) {
                System.out.print("\t\t\tsrc: " + tr.source.state + ", Label: ");
                if (tr.guard.size() > 0 || !tr.event.equals("")) {
                    System.out.print(tr.event + " [");
                    for (String gd : tr.guard) {
                        System.out.print("" + gd + "");
                    }
                    System.out.print("]");
                } else {
                    System.out.print("1");
                }
                System.out.println(", tgt: " + tr.target.state);
            } 
        }
    }

    static void printAlgos (Set <EnforcerAlgo> algos) {
        System.out.println("\nAlgorithms:");
        for (EnforcerAlgo alg: algos) {
            System.out.print("\t" + alg.ID);
            System.out.println(": | " + alg.algo + " |");
        }
    }

    static void printXML (EnforcerFB efb) {
        try {
            File inputFile = new File("TEMPLATE.fbt");
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(inputFile);
   
            // update FB name attribute
            Node type = doc.getElementsByTagName("FBType").item(0);
            NamedNodeMap attr = type.getAttributes();
            Node nodeAttr = attr.getNamedItem("Name");
            nodeAttr.setTextContent(efb.name);
            nodeAttr = attr.getNamedItem("Comment");
            nodeAttr.setTextContent("This is an enforcer FB");
   
            // add interfaces
            Element root = (Element) doc.getElementsByTagName("EventInputs").item(0);
            for (String evItf : efb.interfaces.eventInputs) {
                Element newEvent = doc.createElement("Event");
                newEvent.setAttribute("Name", evItf);
                newEvent.setAttribute("Type", "Event");
                newEvent.setAttribute("Comment", "");
                for (With wth: efb.interfaces.withInputs) {
                    if (wth.event.equals(evItf)) {
                        Element newWith = doc.createElement("With");
                        newWith.setAttribute("Var", wth.data);
                        newEvent.appendChild(newWith);
                    }
                }
                root.appendChild(newEvent);
            }
            root = (Element) doc.getElementsByTagName("EventOutputs").item(0);
            for (String evItf : efb.interfaces.eventOutputs) {
                Element newEvent = doc.createElement("Event");
                newEvent.setAttribute("Name", evItf);
                newEvent.setAttribute("Type", "Event");
                newEvent.setAttribute("Comment", "");
                for (With wth: efb.interfaces.withOutputs) {
                    if (wth.event.equals(evItf)) {
                        Element newWith = doc.createElement("With");
                        newWith.setAttribute("Var", wth.data);
                        newEvent.appendChild(newWith);
                    }
                }
                root.appendChild(newEvent);
            }
            root = (Element) doc.getElementsByTagName("InputVars").item(0);
            for (String dtItf : efb.interfaces.dataInputs) {
                Element newData = doc.createElement("VarDeclaration");
                newData.setAttribute("Name", dtItf);
                newData.setAttribute("Type", "BOOL");
                newData.setAttribute("Comment", "");
                root.appendChild(newData);
            }
            root = (Element) doc.getElementsByTagName("OutputVars").item(0);
            for (String dtItf : efb.interfaces.dataOutputs) {
                Element newData = doc.createElement("VarDeclaration");
                newData.setAttribute("Name", dtItf);
                newData.setAttribute("Type", "BOOL");
                newData.setAttribute("Comment", "");
                root.appendChild(newData);
            }

            // add ECC
            root = (Element) doc.getElementsByTagName("ECC").item(0);
            double xCoor = 0.0;
            for (EnforcerState efState : efb.ecc.keySet()) {
                Element newState = doc.createElement("ECState");
                newState.setAttribute("Name", "S"+efState.state);
                newState.setAttribute("x", "" + xCoor);
                newState.setAttribute("y", "0.0");
                if (efState.state == 0) {
                    newState.setAttribute("Comment", "Initial State");
                } else {
                    newState.setAttribute("Comment", "");
                }
                if (efState.actions.size()>0) {
                    for (EnforcerAction efAction : efState.actions) {
                        Element newAction = doc.createElement("ECAction");
                        newAction.setAttribute("Algorithm", efAction.algoID);
                        newAction.setAttribute("Output", efAction.eventOut);
                        newState.appendChild(newAction);
                    }
                }
                root.appendChild(newState);
                xCoor = xCoor + 100.0;
            }
            xCoor = 0.0;
            for (EnforcerState efState : efb.ecc.keySet()) {
                for (EnforcerTransition efTrans  : efb.ecc.get(efState)) {
                    Element newTrans = doc.createElement("ECTransition");
                    newTrans.setAttribute("Source", "S"+efTrans.source.state);
                    newTrans.setAttribute("Destination", "S"+efTrans.target.state);
                    String tmpCondition = efTrans.event + "[";
                    for (String efGuard : efTrans.guard) {
                        tmpCondition = tmpCondition + efGuard;
                    }
                    tmpCondition = tmpCondition+"]";
                    if(efTrans.guard.size() == 0) {
                        tmpCondition = "1";
                    }
                    newTrans.setAttribute("Condition", tmpCondition);
                    newTrans.setAttribute("Comments", "");
                    newTrans.setAttribute("x", ""+xCoor);
                    newTrans.setAttribute("y", "");
                    xCoor = xCoor + 100.0;
                    root.appendChild(newTrans);
                }
            }
            
            // add algorithm
            root = (Element) doc.getElementsByTagName("BasicFB").item(0);
            for (EnforcerAlgo algo : efb.algorithms) {
                Element newAlgo = doc.createElement("Algorithm");
                newAlgo.setAttribute("Name", algo.ID);
                newAlgo.setAttribute("Comment", "");
                Element newST = doc.createElement("ST");
                Node cdata = doc.createCDATASection(algo.algo);
                newST.appendChild(cdata);
                newAlgo.appendChild(newST);
                root.appendChild(newAlgo);
            }

            // remove spaces
            XPath xp = XPathFactory.newInstance().newXPath();
            NodeList nl = (NodeList) xp.evaluate("//text()[normalize-space(.)='']", doc, XPathConstants.NODESET);
            for (int i=0; i < nl.getLength(); ++i) {
               Node node = nl.item(i);
               node.getParentNode().removeChild(node);
            }
   
            // write the content to new xml
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource source = new DOMSource(doc);
            System.out.println("\nOutput Enforcer FB: " + efb.name + ".fbt");
            StreamResult consoleResult = new StreamResult(new File(efb.name+".fbt"));
            transformer.transform(source, consoleResult);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
        String fileName = args[0];
        String propMeta = "";
        int tmpSource = 0;
        String tmpEvent = "";
        Set <String> tmpGuards = new HashSet<String>();
        int tmpTarget = 0;
        String tmpType = "";
        String [] tmpLabel;
        Set <Transition> tmpTrans;
        String[] arrLines;
        
        // Input property
        Map <Integer, Set <Transition>> propMap = new HashMap <Integer, Set <Transition>>();
        try(BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line = br.readLine();
            propMeta = line;
            while (true) {
                line = br.readLine();
                if (line != null){
                    arrLines = line.replace(" ", "").split(",");
                    tmpLabel = arrLines[1].split("\\.");
                    tmpSource = Integer.parseInt(arrLines[0]);
                    tmpEvent = arrLines[1].split("\\.")[0];
                    tmpGuards = new HashSet<String>();
                    tmpGuards.addAll(
                        new HashSet<>(
                            Arrays.asList(
                                (tmpLabel[1].replace("{", "").replace("}", "").replace(" ", "")).split("-"))));
                    tmpTarget = Integer.parseInt(arrLines[2]);
                    tmpTrans = new HashSet<Transition>();         
                    if (propMap.containsKey(tmpSource)) {
                        tmpTrans.addAll(propMap.get(tmpSource));
                    }
                    tmpType = "";
                    if (tmpLabel.length == 3) {
                        tmpType = tmpLabel[2].replace("(", "").replace(")", "").replace(" ", "");
                    }
                    tmpTrans.add(new Transition(tmpSource, tmpEvent, tmpGuards, tmpTarget, tmpType));
                    propMap.put(tmpSource, tmpTrans);

                    if(!propMap.containsKey(tmpTarget)) {
                        propMap.put(tmpTarget, new HashSet<Transition>());
                    }
                } else { break; }
            }
        }
        propMeta = propMeta.replace("(", "").replace(")", "");
        System.out.println("============================================\nINPUT\n");
        System.out.println("Property Name: " + propMeta);
        for (Integer st : propMap.keySet()) {
            System.out.println("\n\tState " + st);
            for (Transition tr : propMap.get(st)) {
                System.out.print("\t\tSrc: " + tr.source + ", Label: " + tr.event + "{");
                for (String gd : tr.guard) {
                    System.out.print(" " + gd +" ");
                }
                System.out.print("}, Tgt: " + tr.target);
                System.out.print(", Type: " + tr.type);
                System.out.println();
            }
        }

        // Build Interfaces
        Set <String> myinputevents = new HashSet<String>();
        Set <String> myoutputevents = new HashSet<String>();
        Set <String> myinputdata = new HashSet<String>();
        Set <String> myoutputdata = new HashSet<String>();
        Set <With> mywithinput = new HashSet<With>();
        Set <With> mywithoutput = new HashSet<With>();
        for (Integer st : propMap.keySet()) {
            for (Transition tr : propMap.get(st)) {
                myinputevents.add(tr.event+"_I");
                myoutputevents.add(tr.event+"_O");
                for (String gd : tr.guard) {
                    myinputdata.add(gd.split("=")[0]+"_I");
                    myoutputdata.add(gd.split("=")[0]+"_O");
                    mywithinput.add(new With(tr.event+"_I", gd.split("=")[0]+"_I"));
                    mywithoutput.add(new With(tr.event+"_O", gd.split("=")[0]+"_O"));
                }
            }
        }
        EnforcerInterfaces myinterfaces = new EnforcerInterfaces (
            myinputevents, myoutputevents, myinputdata, myoutputdata,
            mywithinput, mywithoutput);
        
        // Build ECC and algos
        Map <EnforcerState, Set <EnforcerTransition>> ecc = new HashMap <EnforcerState, Set <EnforcerTransition>>();
        Set <EnforcerAlgo> algos = new HashSet<EnforcerAlgo>();
        buildECC(propMap, new EnforcerState(new HashSet<EnforcerAction>()), 0, ecc, new HashSet<Integer>(), new HashMap<Integer, EnforcerState>(), algos);
        EnforcerFB enforcerFB = new EnforcerFB(propMeta+"EnforcerFB", myinterfaces, ecc, algos);
        
        // prints
        System.out.println("\n============================================\nOUTPUT\n");
        System.out.println("Enforcer FB Name: " + enforcerFB.name);
        System.out.println("\nInterfaces:");
        printInterfaces("Input Events", enforcerFB.interfaces.eventInputs);
        printInterfaces("Output Events", enforcerFB.interfaces.eventOutputs);
        printInterfaces("Input Data", enforcerFB.interfaces.dataInputs);
        printInterfaces("Output Data", enforcerFB.interfaces.dataOutputs);
        printWiths("Input With", enforcerFB.interfaces.withInputs);
        printWiths("Output With", enforcerFB.interfaces.withOutputs);
        printECC(enforcerFB.ecc);
        printAlgos(enforcerFB.algorithms);
        printXML(enforcerFB);
    }
}