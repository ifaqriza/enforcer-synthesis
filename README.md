- synthesis-source-code contains the source code of enforcer synthesis
No need to compile, simply type:
java enforcer <.txt file of a property automaton (see example-properties for template)>

- example-applications contains the conveyor test station case study. It is implemented using 4DIAC-IDE.
An installation is required to run the case study (see https://www.eclipse.org/4diac/en_ide.php)

- example-enforcers contains synthesised enforcers in XML format

- example-properties contains example of properties